Docker Setup
============

This Docker image is for cross-compiling software to run on the Eagle 8074 board (Snapdragon 801). It is suitable for building aDSP/qurt libs using the DSPAL/HEXAGON SDK as well as Krait/Linux apps/libs using an ARM GCC compiler.
