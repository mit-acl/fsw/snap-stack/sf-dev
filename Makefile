IMAGE:="mitacl/sf-cross"

.PHONY: all
all: cross

.PHONY: cross
cross:
	@docker build -t $(IMAGE) -f docker/Dockerfile.cross .

.PHONY: cross-bash
cross-bash:
	@docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
		-e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
		$(IMAGE) /bin/bash

.PHONY: mini-dm
mini-dm:
	@docker run --rm -it --privileged -u 0 \
		$(IMAGE) /bin/bash -c "\$${HEXAGON_SDK_ROOT}/tools/debug/mini-dm/Linux_Debug/mini-dm"
