Snapdragon Flight 801 - Development Environment
===============================================

This collection of scripts along with Docker creates an isolated cross-compilation environment for the Eagle 8074 (Snapdragon 801) board. A single Docker image is built with the Qualcomm Hexagon SDK for DSP/QuRT development, as well as an ARM GCC 4.9 toolchain for Krait/Linux development (including the Linux sysroot provided by `qrlSDK`).

**ACL Users**: You can download the files below from [this](https://www.dropbox.com/sh/1553npmpuw3ha2w/AACk809EGnO22jmUHT_Oh3tYa?dl=0) password-protected link (hint: sikorsky).

## Initial Setup

1. Download `Flight_3.1.3.1_JFlash.zip` from Intrinsyc and unzip.
2. Power on and connect Snapdragon Flight 801 board and run `sudo ./jflash.sh`. Wait for LED to turn blue (reboot).
3. Download `Flight_3.1.3.1_qcom_flight_controller_hexagon_sdk_add_on.zip` from Intrinsyc and unzip.
4. Run `./installfcaddon.sh`. Wait for LED to turn blue (reboot).
5. (Optional) Place `snapdragon-flight-license.bin` and `mv_1.1.9_8x74.deb` into `setup` directory. This is for the machine vision library.
6. ACL setup: `cd setup` and `./setup.bash`

## Environment Setup - Docker

0. [Install docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and follow the [post-installation steps](https://docs.docker.com/install/linux/linux-postinstall/) to allow non-root docker usage.
1. Download [qualcomm_hexagon_sdk_lnx_3_0_eval.bin](https://developer.qualcomm.com/download/hexagon/hexagon-sdk-v3-linux.bin) from Qualcomm.
2. Download [Flight_3.1.3_qrlSDK.tgz](https://tech.intrinsyc.com/projects/snapdragon-flight/files) from Intrinsyc.
3. Move `qualcomm_hexagon_sdk_lnx_3_0_eval.bin` and `Flight_3.1.3_qrlSDK.tgz` into the `workspace` subdirectory of this project.
4. In the root directory of this project (e.g., `~/sf-dev/`), run `make cross`.
5. The `mitacl/sf-cross` image will be created.

## Usage

One the Docker images have been created, the `build.sh` script is used to cross-compile packages in the `workspace` subdirectory. Clone projects that you would like to cross-compile for the DSP into the `workspace` subdirectory. Examples are given below.

### Examples

```bash
# Build and push dspal_tester and version_test
~/sf-dev $ ./build.sh workspace/dspal/test --load
```

```bash
# Build and push esc_interface
~/sf-dev $ ./build.sh workspace/esc_interface --load
```

```bash
# Drop into the Docker cross-compilation environment
~/sf-dev $ ./build.sh --bash
```

