#!/bin/bash

IMAGE=mitacl/sf-cross

function usage() {
    echo
    echo -e "\t$0 <path> [--bash]"
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=
    LONGOPTS=bash,target:,load,clean

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # now enjoy the options in order and nicely split until we see --
    while true; do
        case "$1" in
            --bash)
                bsh=y
                shift
                ;;
            --target)
                target="$2"
                shift 2
                ;;
            --load)
                target="load"
                shift
                ;;
            --clean)
                target="clean"
                shift
                ;;
            --)
                shift
                break
                ;;
            *)
                echo "Invalid options"
                usage
                exit 3
                ;;
        esac
    done

    # handle non-option arguments
    if [[ "$bsh" == "n" && $# -ne 1 ]]; then
        echo "$0: Please specify the build path"
        usage
        exit 4
    fi

    buildpath=$1
}

bsh=n
parseargs "$@"

SCRIPTSDIR=scripts
RUNSCRIPT="$SCRIPTSDIR/docker_builder.sh"

if [[ "$bsh" == "y" ]]; then
    RUNSCRIPT=
    buildpath=
    target=
fi

# n.b.: using the host network allows us to use adb in the container
# without killing the adb-server on the host machine.

docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
        -e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
        -v `pwd`/workspace:/home/cross/workspace:rw \
        -v `pwd`/scripts:/home/cross/scripts:rw \
        -v /dev/bus/usb:/dev/bus/usb \
        --network=host \
        ${IMAGE} /bin/bash $RUNSCRIPT $buildpath $target
