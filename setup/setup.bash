#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source $MYPATH/utils

echo
echo_yel "\tRemember to first flash the OS and push DSP images."
echo_yel "\tThis can be done using jflash.sh and installfcaddon.sh."
echo_yel "\t\t      Use version Flight_3.1.3.1."
echo

read -p $'\e[1;37mVehicle type (e.g., `HX`): \e[0m' VEHICLE_TYPE
read -p $'\e[1;37mVehicle num (e.g., `01`): \e[0m' VEHICLE_NUM
VEHICLE="$VEHICLE_TYPE$VEHICLE_NUM"
SIKORSKY_IP="192.168.0.19"
HOME=/home/linaro

# First, verify that there is a Snapdragon plugged in; exit if not
findQualcommDevice

# Make sure that this is a Snapdragon Flight (Eagle 801 / APQ8074)
if [[ $(adb shell uname -a) != *"eagle8074"* ]]; then
  fail "Please run while connected to a Snapdragon Flight 801 board"
  exit
fi

echo

#
# Setup bashrc / home environment
#

if [[ $(adb shell cat $HOME/.bashrc) != *"export HOME"* ]]; then

    message "Pushing .bashrc"

    adb push $MYPATH/bashrc $HOME/bashrc
    adb shell "sed -i s/_ROSMASTER_/$SIKORSKY_IP/g $HOME/bashrc"
    adb shell "sed -i s~_HOME_~$HOME~g $HOME/bashrc"
    adb shell "sed -i s/_VEHTYPE_/$VEHICLE_TYPE/g $HOME/bashrc"
    adb shell "sed -i s/_VEHNUM_/$VEHICLE_NUM/g $HOME/bashrc"
    adb shell "cat $HOME/bashrc >> $HOME/.bashrc"
    adb shell rm $HOME/bashrc

    # ssh sources this (root user), which sources the linaro bashrc
    adb push $MYPATH/root_bashrc /root/.bashrc
    adb shell "sed -i s~_HOME_~$HOME~g /root/.bashrc"

    echo
fi

#
# Setup networking
#
message "Setting up networking for $YELLOW$VEHICLE"

wlan_mode=$(adb shell /usr/local/qr-linux/wificonfig.sh -g | tr -d '[:space:]')
if [[ "$wlan_mode" != "station" ]]; then
    adb shell /usr/local/qr-linux/wificonfig.sh -s station
    adb reboot

    echo_info "Rebooting device into STATION mode..."
    sleep 15
    findQualcommDevice
fi

# Hostname
adb shell "echo $VEHICLE > /etc/hostname"
adb shell "echo 127.0.0.1 localhost $VEHICLE > /etc/hosts"

if [[ $(adb shell cat /etc/wpa_supplicant/wpa_supplicant.conf) != *"mit-acl"* ]]; then

    # SSID / passphrase to robot wifi
    read -p $'\e[1;37mHighbay SSID: \e[0m' netssid
    read -p $'\e[1;37mSSID Passphrase: \e[0m' netpsk

    # WPA supplicant for wifi comms
    adb push $MYPATH/wpa_supplicant /etc/wpa_supplicant/wpa_supplicant.conf
    adb shell "sed -i s/_NETSSID_/$netssid/g /etc/wpa_supplicant/wpa_supplicant.conf"
    adb shell "sed -i s/_NETPSK_/$netpsk/g /etc/wpa_supplicant/wpa_supplicant.conf"
fi

echo_info "Waiting for connection to mitnet"
adb shell wpa_cli reconfigure
adb shell wpa_cli sel 0
adb shell wpa_cli stat
sleep 15
adb shell wpa_cli stat

if [[ $(adb shell cat /etc/network/interfaces) != *"wlan0"* ]]; then
    # Make sure to add wifi power saving stuff (wireless-power off)
    adb shell "echo 'auto wlan0' >> /etc/network/interfaces"
    adb shell "echo 'iface wlan0 inet dhcp' >> /etc/network/interfaces"
    adb shell "echo 'wireless-power off' >> /etc/network/interfaces"
fi

echo

#
# Setup SSH
#
message "Setting up SSH for $YELLOW$VEHICLE"

adb shell "sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config"

echo

#
# Setup ntpd
#
message "Setting up ntpd for $YELLOW$VEHICLE"

# ntpdate
adb shell "echo 'server $SIKORSKY_IP' > /etc/ntp.conf"
adb shell "echo 'server ntp.ubuntu.com' >> /etc/ntp.conf"

echo

#
# Set root password to 'linaro'
#

adb shell "echo 'root:linaro' | chpasswd"

#
# Update system
#
message "Updating apt-get sources and packages on $YELLOW$VEHICLE"

# uninstall fastcv-internal
adb shell "apt-get install -f --yes"

if [[ $(adb shell cat /etc/apt/sources.list) != *"trusty-updates"* ]]; then
    # add trusty-updates
    adb shell "echo 'deb http://ports.ubuntu.com/ubuntu-ports/ trusty-updates main universe' >> /etc/apt/sources.list"
    adb shell "echo 'deb-src http://ports.ubuntu.com/ubuntu-ports/ trusty-updates main universe' >> /etc/apt/sources.list"
fi

# control.tar.xz before control.tar.gz -- upgrade dpkg
# https://github.com/ros/rosdistro/issues/19481#issuecomment-442802669

adb shell "apt-get update"
adb shell "apt-get upgrade -o Dpkg::Options::='--force-confdef' --yes --force-yes"
# the force-condef is for accepting the defaults for overwritting the sudoers file

#
# Install ROS base
#
message "Installing ROS Indigo on $YELLOW$VEHICLE"

# set locale
adb shell "update-locale LANG=C LANGUAGE=C LC_ALL=C LC_MESSAGES=POSIX"

# setup sources / keys
adb shell "echo 'deb http://packages.ros.org/ros/ubuntu trusty main' > /etc/apt/sources.list.d/ros-latest.list"
adb shell "apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654"

adb shell "apt-get update"
adb shell "apt-get install ros-indigo-ros-base --yes"
adb shell "apt-get -f -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-overwrite' install --yes" # fontconfig fix
adb shell "apt-get install ros-indigo-tf2-geometry-msgs ros-indigo-eigen-conversions --yes"

# ros dependency manager
adb shell "rosdep init"
adb shell "rosdep update"

echo

#
# Install Avahi
#
message "Installing Avahi for $YELLOW$VEHICLE$CYAN.local discovery"

adb shell "apt-get install avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan --yes"

echo

#
# Install goodies
#
message "Installing extras for $YELLOW$VEHICLE"

adb shell "apt-get install htop libncurses5-dev python-pip python-catkin-tools --yes"
# adb shell "pip2 install catkin-tools"

echo

#
# Setup acl_ws
#

# ACL_WS_EXISTS=$(adb shell "if [ -d '/home/linaro/acl_ws' ]; then echo 'exists'; else echo ''; fi;")
# if [[ "$ACL_WS_EXISTS" != "exists" ]]; then
#     message "Setting up acl_ws for $YELLOW$VEHICLE"

#     adb shell mkdir -p $HOME/acl_ws/src
#     adb shell "cd $HOME/acl_ws/src && git clone https://bitbucket.org/brettlopez/snap.git"
#     adb shell "cd $HOME/acl_ws/src && git clone https://bitbucket.org/brettlopez/acl-system.git"
#     adb shell "cd $HOME && chmod -R o-w acl_ws"

#     warning "You'll need to do the rest yourself (private acl repos)."

#     echo
# fi

#
# If present in this dir, install license and mv lib
#

SFLIC=snapdragon-flight-license.bin
SFMV=mv_1.2.7_8x74.deb

if [[ -f $MYPATH/$SFLIC ]]; then
    message "Pushing $SFLIC to $YELLOW$VEHICLE"

    adb push $MYPATH/$SFLIC /usr/lib/
    adb shell sync

    echo
fi

if [[ -f $MYPATH/$SFMV ]]; then
    message "Pushing $SFMV on $YELLOW$VEHICLE"

    adb push $MYPATH/$SFMV $HOME
    # adb shell "dpkg -i $HOME/$SFMV"
    # adb shell "rm $HOME/SFMV"
    warning "You'll need to 'dpkg -i $SFMV' yourself."

    echo
fi

#
# Push git commit hash to vehicle so we know what version was used to setup
#

GIT_VERSION_STRING=$(cd $MYPATH ; git describe --tags --abbrev=8 --always --dirty --long)
adb shell "echo '$GIT_VERSION_STRING' > /sf-dev-setup"

# -----------------------------------------------------------------------------

# Success!
success "Setup completed -- rebooting"
adb reboot

